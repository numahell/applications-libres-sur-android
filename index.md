---
title: "Des applications libres pour Android"
order: 0
in_menu: true
---
Les applications libres pour smartphone android

## Messagerie


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/FairEmail.png">
    </div>
    <div>
      <h3>FairEmail</h3>
      <p>Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/fairemail.html">Vers la notice Framalibre</a>
        <a href="https://email.faircode.eu/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h3>Signal</h3>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>


## Vidéos, Musique et podcasts


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/NewPipe.png">
    </div>
    <div>
      <h3>NewPipe</h3>
      <p>Client multimédia YouTube, PeerTube, SoundCloud &amp; MediaCCC libre pour Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/newpipe.html">Vers la notice Framalibre</a>
        <a href="https://newpipe.schabi.org/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h3>VLC</h3>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/AntennaPod.png">
    </div>
    <div>
      <h2>AntennaPod</h2>
      <p>Un gestionnaire de Podcast pour Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/antennapod.html">Vers la notice Framalibre</a>
        <a href="http://antennapod.org/">Vers le site</a>
      </div>
    </div>
  </article>


## Cartographie et mobilité


  <article class="framalibre-notice">
    <div>
      <img src="">
    </div>
    <div>
      <h3>Organic Maps</h3>
      <p>Application libre de cartes hors ligne et navigation GPS basée sur les données OpenStreetMap - Pour Android et iOS</p>
      <div>
        <a href="https://beta.framalibre.org/notices/organic-maps.html">Vers la notice Framalibre</a>
        <a href="https://organicmaps.app/fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Transportr.png">
    </div>
    <div>
      <h3>Transportr</h3>
      <p>Transportr est une appli conçue pour faciliter autant que possible l’utilisation des transports en commun, pour tous.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/transportr.html">Vers la notice Framalibre</a>
        <a href="https://transportr.app/">Vers le site</a>
      </div>
    </div>
  </article>

## Sécurité et vie privée


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Exodus%20Privacy.png">
    </div>
    <div>
      <h3>Exodus Privacy</h3>
      <p>εxodus analyse les applications Android dans le but de lister les pisteurs embarqués.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/exodus-privacy.html">Vers la notice Framalibre</a>
        <a href="https://exodus-privacy.eu.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/KeePassDX.png">
    </div>
    <div>
      <h3>KeePassDX</h3>
      <p>Keepass DX est un client KeePass Android de gestion d'identités numériques et de mots de passe.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/keepassdx.html">Vers la notice Framalibre</a>
        <a href="https://f-droid.org/packages/com.kunzisoft.keepass.libre/">Vers le site</a>
      </div>
    </div>
  </article> 